package com.thread.demo;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

public class MultiThreadExecutorWithOutSyncronisedDemo {
	

	 static int  count=0;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//int count=0;
		MultiThreadExecutorWithOutSyncronisedDemo mtes=new MultiThreadExecutorWithOutSyncronisedDemo();
		ExecutorService executor = Executors.newFixedThreadPool(2);

		IntStream.range(0, 10000)
		    .forEach(i -> executor.submit(mtes::withOutSynincrement));
		
		IntStream.range(0, 10000)
	    .forEach(i -> executor.submit(mtes::withSynincrement));

		executor.shutdown();
	}
	
	  void withOutSynincrement() {
	    this.count = count + 1;
	    System.out.println(count);
	}

	  synchronized void withSynincrement() {
		    this.count = count + 1;
		    System.out.println(count);
		}
}
