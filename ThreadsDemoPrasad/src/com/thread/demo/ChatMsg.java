package com.thread.demo;

public class ChatMsg {
	
	boolean flag = false;

	   public synchronized void send(String msg) {
	      if (flag) {
	         try {
	            wait();
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }
	      }
	      System.out.println(msg);
	      flag = true;
	      notify();
	   }

	   public synchronized void replay(String msg) {
	      if (!flag) {
	         try {
	            wait();
	         } catch (InterruptedException e) {
	            e.printStackTrace();
	         }
	      }

	      System.out.println(msg);
	      flag = false;
	      notify();
	   }

}
