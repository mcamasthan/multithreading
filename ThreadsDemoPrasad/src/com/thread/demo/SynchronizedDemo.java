package com.thread.demo;

public class SynchronizedDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Calculate calculate=new Calculate();
		Master m= new Master(calculate,"master");
		Student s=new Student(calculate,"student");
		m.start();
		s.start();
	}
	
	

}

class Calculate{
	public  synchronized void  testCalculation(String name){
		for(int i=0;i<5;i++){
			System.out.println(Thread.currentThread().getName()+"==="+name+i);
		}
	}
}

class Master extends Thread{
	
	Calculate c;
	String name="";
	Master( Calculate c,String name){
		this.c=c;
		this.name=name;
	}
	public void run(){
		c.testCalculation(name);
	}
}
class Student extends Thread{
	Calculate c;
	String name="";
	Student( Calculate c,String name){
		this.c=c;
		this.name=name;
	}
	public void run(){
		c.testCalculation(name);
	}
}
