package com.thread.demo;

public class Friend1 implements Runnable {
	
	ChatMsg msg;
	   String[] s1 = { "Hi", "How are you ?", "I am also doing fine!","Bye" };

	   public Friend1(ChatMsg msg) {
	      this.msg = msg;
	   }

	   public void run() {
	      for (int i = 0; i < s1.length; i++) {
	    	  msg.send(Thread.currentThread().getName()+"  typing... "+s1[i]);
	      }
	   }

}
