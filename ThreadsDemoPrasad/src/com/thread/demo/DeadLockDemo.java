package com.thread.demo;

public class DeadLockDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/*final String  resource="Masthan";
	final 	String resource1="Prasad";
		
		Thread t=new Thread(){
			public void run(){
				synchronized (resource) {
					 System.out.println("Thread 1: locked resource ");  
				}
				  try { Thread.sleep(100);} catch (Exception e) {}  
				  synchronized (resource1) {
						 System.out.println("Thread 1: locked resource1 ");  
					}
			}
		};
		
		Thread t1=new Thread(){
			public void run(){
				synchronized (resource) {
					 System.out.println("Thread 2: locked resource1 ");  
				}
				  try { Thread.sleep(100);} catch (Exception e) {}  
				  synchronized (resource1) {
						 System.out.println("Thread 2: locked resource ");  
					}
			}
		};
		
		t.start();
		t1.start();*/
		
	 String lock1 = "ratan jaiswal";  
	    String lock2 = "vimal jaiswal";  
	    // t1 tries to lock resource1 then resource2  
	    Thread t1 = new Thread() {  
	      public void run() {  
	    	  synchronized (lock1) {
	                System.out.println("Thread-1 acquired lock1");
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException e) {
	                    System.out.println("Thread-1 interrupted.");
	                }
	                System.out.println("Thread-1 waiting for lock2");
	                synchronized (lock2) {
	                    System.out.println("Thread-1 acquired lock2");
	                    try {
	                        Thread.sleep(1000);
	                    } catch (InterruptedException e) {
	                        System.out.println("Thread-1 interrupted.");
	                    }
	                }
	                System.out.println("Thread-1 releases lock2");
	            }
	            System.out.println("Thread-1 releases lock1");
	      }  
	    };  
	  
	    // t2 tries to lock resource2 then resource1  
	    Thread t2 = new Thread() {  
	      public void run() {  
	    	  synchronized (lock1) {
	                System.out.println("Thread-2 acquired lock1");
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException e) {
	                    System.out.println("Thread-2 interrupted.");
	                }
	                System.out.println("Thread-2 waiting for lock1");
	                synchronized (lock2){
	                    System.out.println("Thread-2 acquired lock2");
	                    try {
	                        Thread.sleep(1000);
	                    } catch (InterruptedException e) {
	                        System.out.println("Thread-2 interrupted.");
	                    }
	                }
	                System.out.println("Thread-2 releases lock2");
	            }
	            System.out.println("Thread-2 releases lock1");
	      }  
	    };  
	  
	      
	    t1.start();  
	    t2.start();

	}
	
	

}
