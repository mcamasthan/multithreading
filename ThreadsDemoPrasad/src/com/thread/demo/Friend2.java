package com.thread.demo;

public class Friend2 implements Runnable {
	ChatMsg msg;
	String[] s2 = { "Hi", "I am good, what about you?", "Great!" ,"Bye"};

	   public Friend2(ChatMsg msg) {
	      this.msg = msg;
	   }

	   public void run() {
	      for (int i = 0; i < s2.length; i++) {
	    	  msg.replay(Thread.currentThread().getName()+" typing... "+s2[i]);
	      }
	   }

}
